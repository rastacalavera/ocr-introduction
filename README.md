# OCR Introduction

This is an Open Community Resource. That means that the community contributes information/aid to others in an easily accessible format that can be readily accessed.

This project is emulating the [Open Education Resource Model](https://www.oercommons.org/) on a much smaller scale.

### Project Topics
- [Home Networking](https://gitlab.com/rastacalavera/ocr_home_networking/-/wikis/%5BOCR%5D-Home-Networking-for-the-Curious-and-the-Serious)
- [Open Social Media (Fediverse)](https://gitlab.com/rastacalavera/ocr-social-networking)